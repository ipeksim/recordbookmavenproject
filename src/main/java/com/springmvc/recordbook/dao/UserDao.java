package com.springmvc.recordbook.dao;

import java.util.List;

import com.springmvc.recordbook.model.User;

public interface UserDao {
	
	void register (User user);
	public List <User> getUsers();

}

