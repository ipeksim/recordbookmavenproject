package com.springmvc.recordbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.springmvc.recordbook.dao.UserDao;
import com.springmvc.recordbook.service.UserService;

@Controller
public class RecordsController {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	public UserService userService;
	
	
    @RequestMapping(value = "/showrecords", method = RequestMethod.GET)
    public String getUsers(ModelMap userModel)
    {
     userModel.addAttribute("users",userService.getUsers());
        
        return "showrecords";
    }
    

}