package com.springmvc.recordbook.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.springmvc.recordbook.dao.UserDao;
import com.springmvc.recordbook.model.User;

public class UserServiceImpl implements UserService{
	
	@Autowired
	UserDao userDao;
	
	public void register(User user) {
		System.out.println(" "+user);
		userDao.register(user);
	}
	
	public List <User> getUsers(){
		return userDao.getUsers();
	}

}
