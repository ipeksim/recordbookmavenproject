package com.springmvc.recordbook.service;

import java.util.List;

import com.springmvc.recordbook.model.User;

public interface UserService {
	

	void register(User user);
	
	public List <User> getUsers();

}
